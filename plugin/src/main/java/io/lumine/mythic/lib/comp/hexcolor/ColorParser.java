package io.lumine.mythic.lib.comp.hexcolor;

public interface ColorParser {

    /**
     * @param format
     *            The string from which to parse color codes
     * @return String with parsed color codes
     */
    String parseColorCodes(String format);
}
